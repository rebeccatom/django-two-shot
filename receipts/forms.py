from django import forms
from django.forms import models
from receipts.models import Receipt, ExpenseCategory, Account

class CreateReceiptForm(models.ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

class ExpenseCategoryForm(models.ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]

class AccountForm(models.ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
