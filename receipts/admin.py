from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    # should I have id here in the list display?
    pass
@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    # should I have id here in the list display?
    pass
@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    # should I have id here in the list display?
    pass
